<h1>Ejercicio:</h1>

Realizar una calculadora que pueda sumar, restar, dividir y multiplicar 2 numeros hexadecimales.

- No se valora diseño
- Se valora el uso de PHP orientado a objetos.

<h1>Resolución:</h1>

## Archivo 'index.html':
Este archivo contiene el frontend de la aplicación, el cual incluye al archivo: <b>Calculator.js</b>, quien se encarga de la comunicación con el backend de la aplicación, esto se realiza de forma asíncrona.

## Directorio 'calculator'
Aquí se encuentra el archivo <b>'index.php'</b>, el cual incluye la función 'autoload' y las 'rutas' del proyecto. Además de crear una instancia de la clase <b>App</b>.

## autoload
El archivo <b>autoload.php</b> se encarga de incluir las clases a medida que son requeridas durante la ejecución de la aplicación.

## rutas
El archivo <b>routes.php</b> retorna un array asociativo en el cuál se registrarán los distintos endpoints de la aplicación. Su estructura es la siguiente: 
<code>['MÉTODO_HTTP' => ['NOMBRE_DE_RUTA' => 'CLASE_CONTROLADOR@MÉTODO_CLASE', ...], ...]</code>

<h1>Directorio Core:</h1>

## clase App
Esta clase se encarga de crear una instance de <b>RoutesResolver</b>, pasandole como argumentos el arreglo de rutas y una instancia del singleton <b>Request</b>.

## clase RoutesResolver
Posee 4 métodos (además de su constructor),
<code>route_exists()</code>, el cual verifíca la existencia de la ruta con la cuál se está intentando comunicar el frontend.
<code>controller_exists($class)</code>, el cual verificará la existencia del controlador.
<code>resolve()</code>, este método se encarga de enviar una respuesta con su respectivo código de estado, utilizando la clase utilitaria: <b>Response</b>, el mensaje de respuesta, se obtiene luego de aplicar la lógica del controlador.

## clase Response
Clase utilitaria, retorna una respuesta (con formato json) y su respectivo código de estado. 

## clase Request
Esta clase se encarga de obtener los datos necesarios de la petición (método de petición, servicio requerido, cabecera de respuesta), y validar los inputs posteriormente utilizados por los controladores.

<h1>Directorio App:</h1>
En este directorio se encuentran los controladores y clases utilizadas por ellos, para resolver el ejercio se utilizó un controlador <b>CalculatorController.php</b>, el cuál utiliza Las siguientes clases (ordenadas por jerarquía):

+ Calculator 
+ Operation  
    |----->Division
    |----->Multiplication
    |----->Subtraction
    |----->Sum
+ OperationType

La clase <h3>Calculator</h3> espera una instancia de <h3>Operation</h3> con la base numérica necesaria, esta misma clase se encargará de setear los factores requeridos, utilizando el método <code>setFactors($a, $b)</code>. Una vez seteados los datos, se procederá a implementar el método <code>calculate()</code>, para obtener una instancia de la sub-clase <b><Division, Multiplication, Subtraction, Sum></b> se invocará: <code>OperationType::getOperatorClass()</code>, quien tomará la decisión según el operador enviado en la petición.