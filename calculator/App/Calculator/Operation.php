<?php 
    namespace App\Calculator;

    abstract class Operation
    {

        protected 
            $base, 
            $factors;

        public function setBase($base) {
            $this->base = $base;
        }

        public function setFactors($a, $b) {
            $base = $this->base;
            $this->factors = [intval($a, $base), intval($b, $base)];
        }

        public abstract function calculate();

    }