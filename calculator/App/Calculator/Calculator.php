<?php
    namespace App\Calculator;

    class Calculator
    {
        private $operation;

        public function __construct(Operation $operation, $base) {
            $this->operation = $operation;
            $this->operation->setBase($base);
        }

        public function setFactors($a, $b) {
            $this->operation->setFactors($a, $b);
        }

        public function calculate() {
            return $this->operation->calculate();
        }

    }