<?php
    namespace App\Calculator\Operations;

    use App\Calculator\Operation;

    class Division extends Operation
    {
        public function calculate() {
            if($this->factors[1] == 0)
                return 'Division by zero';
            $result = $this->factors[0] / $this->factors[1];
            if(is_float($result)){
                $result = round($result, 3);
                $integer = intval($result);
                return [
                    'int' => $integer, 
                    'float' => explode('.', strval((float) $result - $integer))[1]
                ];
            }
            return $result;
        }
    }