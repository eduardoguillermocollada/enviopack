<?php
    namespace App\Calculator\Operations;
    
    use App\Calculator\Operation;

    class Multiplication extends Operation
    {  
        public function calculate() {
            return $this->factors[0] * $this->factors[1];
        }
    }