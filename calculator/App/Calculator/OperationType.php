<?php
    namespace App\Calculator;

    use App\Calculator\Operations as Op;

    class OperationType
    {

        private 
            $getValidOperation = [
                '+' => Op\Sum::class,
                '-' => Op\Subtraction::class,
                '*' => Op\Multiplication::class,
                '/' => Op\Division::class,
            ], 
            $operator;

        public function __construct($operator) {
            $this->operator = $operator;
        }

        public static function getOperationType($operator) {
            return (new self($operator))->getOperatorClass();
        }

        private function getOperatorClass() {
            if(isset($this->getValidOperation[$this->operator]))
                return new $this->getValidOperation[$this->operator]();
            else
               return null; 
        }

    }