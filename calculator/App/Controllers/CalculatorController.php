<?php
    namespace App\Controllers;

    use App\Calculator\Calculator;
    use App\Calculator\OperationType;

    class CalculatorController extends Controller
    {
        public function calculate() {
            $this->request->validate(
                [
                    'symbol' => '/^(\+|\-|\*|\/)$/' , 
                    'a'      => '/^[a-fA-F0-9]+$/'  ,
                    'b'      => '/^[a-fA-F0-9]+$/'  ,
                ], 
                [
                    'symbol' => 'Invalid symbol' ,
                    'a'      => 'Invalid number' ,
                    'b'      => 'Invalid number' ,
                ]
            );

            $data = $this->request->validated();
            $calculator = new Calculator(OperationType::getOperationType($data['symbol']), 16);
            $calculator->setFactors($data['a'], $data['b']);
            
            $result = $calculator->calculate();
            if(is_string($result))
                return $result;
            elseif(is_array($result))
                return dechex($result['int']) . '.' . dechex($result['float']);
            else
                return $result < 0 ? '-' . dechex(substr(strval($result), 1)) : dechex($result);
        }

    }