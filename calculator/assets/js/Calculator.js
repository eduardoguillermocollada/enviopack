(function(){
    document.getElementById('Calculate').addEventListener('click', function(){
        var xhr = new XMLHttpRequest(),
            data = {'symbol' : '', 'a' : '', 'b' : ''};
        
        var a = document.getElementById('FirstNumber'),
            b = document.getElementById('SecondNumber'),
            s = document.getElementById('Operator'),
            r = document.querySelector('#Result span');
        
        data['symbol'] = s.value;
        data['a'] = a.value;
        data['b'] = b.value;

        xhr.open('POST', 'calculator/calculate', true);
        xhr.onreadystatechange = function (aEvt) {
            if (xhr.readyState == 4) {
                var response = JSON.parse(xhr.response);
                if(typeof response === 'object') {
                    if(!response['message']) {
                        var message = '';
                        for(var key in response)
                            message += key + ': ' + response[key] + '; ';
                        response['message'] = message;
                    }
                    r.innerText = response['message'];
                }
                else
                    r.innerText = response;
            }
        };
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.send(JSON.stringify(data));
    })
})()