<?php
    namespace Core;

    class App
    {
        private $request, $routes;

        public function __construct(array $routes) {
            $this->request = Request::getInstance();
            $this->routes = $routes;
        }

        public function terminate() {
            $routes = new RoutesResolver($this->request, $this->routes);
            $routes->resolve();
        }
    }