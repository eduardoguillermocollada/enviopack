<?php
    namespace Core;
    
    class Request
    {
        private $uri,
                $method,
                $inputs,
                $http_accept;
        
        private static $instance;

        private $validated, $errors;

        private function __construct() {
            $clean_folder = explode(str_replace(DIRECTORY_SEPARATOR, '\\', $_SERVER['DOCUMENT_ROOT']), INDEXDIR)[1] . '\\';
            $this->method = $_SERVER['REQUEST_METHOD'];
            $this->uri = explode($clean_folder, str_replace('/', '\\', $_SERVER['REQUEST_URI']))[1] ?? '';
            $this->http_accept = $_SERVER['HTTP_ACCEPT'];
            $this->inputs = json_decode(file_get_contents('php://input'), true);
        }

        public static function getInstance() {
            if(!self::$instance)
                self::$instance = new self();
            return self::$instance;
        }

        public function validate(array $rules, array $messages) {
            foreach($rules as $input => $regex) {
                if($this->inputs && array_key_exists($input, $this->inputs)) {
                    if(preg_match($regex, $this->inputs[$input]))
                        $this->validated[$input] = $this->inputs[$input];
                    else
                        $this->errors[$input][] = $messages[$input] ?? 'Error in: ' . $regex;
                }
                else {
                    $this->errors[$input][] = $input . ' is required';
                }
            }
            if($this->errors)
                exit(Response::response($this->errors, 422));
        }

        public function validated() {
            return $this->validated;
        }

        public function __get($property) {
            $accessibles = ['uri', 'method', 'inputs', 'http_accept'];
            if (in_array($property, $accessibles) && 
                property_exists(self::class, $property))
                return $this->$property;
            return null;
        }

    }