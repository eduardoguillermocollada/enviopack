<?php
    namespace Core;

    class Response
    {

        public static function response($response, $status) {
            header('Content-Type: ' . Request::getInstance()->http_accept);
            http_response_code($status);
            if(is_string($response))
                $response = ['message' => $response];
            return json_encode($response);
        }

    }