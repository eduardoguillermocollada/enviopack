<?php
    namespace Core;

    class RoutesResolver
    {
        private $request, $routes;

        public function __construct(Request $request, $routes) {
            $this->request = $request;
            $this->routes = $routes;
        }

        private function route_exists() {
            return isset($this->routes[$this->request->method][$this->request->uri]);
        }

        private function controller_exists($class) {
            return class_exists($class) && is_subclass_of($class, \App\Controllers\Controller::class);
        }

        private function callController() {
            $controller = explode('@', $this->routes[$this->request->method][$this->request->uri]);
            $class = '\\App\\Controllers\\' . $controller[0];
            $method = $controller[1];
            return ($this->controller_exists($class)) ? 
                (new $class())->$method() : 
                exit(Response::response('Unregister class', 500));
        } 

        public function resolve() {
            if(!$this->route_exists())
                exit(Response::response('Route not registered', 404));
            echo Response::response($this->callController(), 200);
        }
    }