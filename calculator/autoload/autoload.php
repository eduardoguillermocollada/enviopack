<?php
    spl_autoload_register(function ($qualified_name) {
        $qualified_name =  str_replace(DIRECTORY_SEPARATOR , '/', $qualified_name) . '.php';
        if(is_file($qualified_name))
            require $qualified_name;
        else {
            require_once 'Core/Response.php';
            exit(Core\Response::response(sprintf('Class: %s not found :(', $qualified_name), 404));
        }
    });