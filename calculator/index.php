<?php
    define('INDEXDIR', dirname(__FILE__));
    
    require 'autoload/autoload.php';
    
    $routes = require 'routes.php';

    (new Core\App($routes))->terminate();